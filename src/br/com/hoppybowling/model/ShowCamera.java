package br.com.hoppybowling.model;

import java.io.IOException;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.hardware.Camera;
import android.hardware.Camera.Parameters;
import android.util.Log;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

public class ShowCamera extends SurfaceView implements SurfaceHolder.Callback {

	private SurfaceHolder hold;
	private Camera cCamera;
	private Context context;
	
	public ShowCamera (Context context, Camera camera) {
		super(context);
		this.context = context;
		cCamera = camera;
		hold = getHolder();
		hold.addCallback(this);
					
		Parameters params = cCamera.getParameters();
		if (params != null){
			Camera.Size minResolution = getMinResolution(params.getSupportedPictureSizes(),params.getPreviewSize());
			if(minResolution != null){
				params.setPreviewSize(minResolution.width, minResolution.height);
				cCamera.setParameters(params);
			}
		}
	}

	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		try {
			cCamera.setPreviewDisplay(hold);
			cCamera.startPreview();
		} catch (IOException e) {

		}

	}

	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width,
			int height) {

	}
	
	private Camera.Size getMinResolution(List<Camera.Size> sizes,Camera.Size tam){
		Camera.Size resolucao = tam;	
		for (Camera.Size s : sizes){
			if ((resolucao.width * resolucao.height) > (s.width * s.height)){
				resolucao = s;
			}
		}
		return resolucao;
	}
	
	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		this.getHolder().removeCallback(this);
		cCamera.stopPreview();
		cCamera.release();
		cCamera = null;
		Log.e("surfaceDestroyed", "surfaceDestroyed");
	}

}
