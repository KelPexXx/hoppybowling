package br.com.hoppybowling.modbus;

import android.os.Handler;
import android.os.Message;
import android.util.Log;
import br.com.hoppybowling.gui.PartidaActivity;

import com.bencatlin.modbusdroid.OldVersion.ModbusMultiLocator;
import com.bencatlin.modbusdroid.OldVersion.ModbusTCPMaster;
import com.serotonin.modbus4j.exception.ModbusInitException;
import com.serotonin.modbus4j.exception.ModbusTransportException;


/**
 * 
 * @author Rafael Werner
 *	Classe para iniciar e gerenciar a comunica��o pelo protocolo Modbus
 */

public class ModbusConnectionManager implements Runnable {
	
	//Log
	private final boolean DEBUGMODE = true;
	private static final String NOMELOG = "ModbusConnectionManager"; 
	
	//Tempo de atualiza��o
	private int c_refreshtime;
	
	//Variavel que armazenara o estado da conex�o
	private boolean c_Connected = false;
	
	//Objetos de conex�o
	private ModbusTCPMaster c_mbTCPMaster;
	private ModbusMultiLocator c_mbLocator;
	private ModbusMultiLocator w_mbLocator;
	
	//Variavel que define se efetuara a escrita 
	private boolean w_mbDoWrite = false;
	
	//Objeto para definir qual tipo de registro
	private Object[] c_mbValues;
	
	//Objeto para escrita
	private Object w_mbValue;
	
	//Handler para troca das mensagens
	private Handler c_Handler;
	
	private PartidaActivity activity;
	/**
	 * 
	 * @param mbMaster
	 * @param mbRefreshtime
	 * @param mbLocator
	 * @param mbHandler
	 * 
	 * Construtor da classe, define o tempo de atualiza��o, objeto mbMaster, o localizador mblocator e o handler da classe principal
	 */
	public ModbusConnectionManager (PartidaActivity activity, ModbusTCPMaster mbMaster, int mbRefreshtime, ModbusMultiLocator mbLocator, Handler mbHandler) {
		this.activity = activity;
		this.c_refreshtime = mbRefreshtime;
		this.c_mbTCPMaster = mbMaster;
		this.c_mbLocator = mbLocator;
		this.c_Handler = mbHandler;
	}
	
	
	/**
	 * 
	 * @param mbLocator
	 * @param mbHandler
	 * 
	 * Metodo para atualizar o localizador e o handler apos altera��o na classe principal
	 */
	public void UpdateThis (ModbusMultiLocator mbLocator, Handler mbHandler) {
		this.c_mbLocator = mbLocator;
		this.c_Handler = mbHandler;
	}

	
	/**
	 * 
	 * @param Refreshtime
	 * 
	 * Metodo para definir o tempo de atualiza��o apos altera��o na classe principal
	 */
	public synchronized void setRefreshTime (int Refreshtime) {
		this.c_refreshtime = Refreshtime;
	}
	
	/**
	 * 
	 * @throws Exception
	 * 
	 * Metodo para conectar ao servidor modbus
	 */
	public synchronized void connect() throws Exception {
		if (this.isConnected()) {
			this.disconnect();
		}
		try {
			//Inicia a conex�o
			c_mbTCPMaster.init();
		}
		catch (ModbusInitException initException) {
			Log.e(NOMELOG,exceptionStringHelper(initException));
			c_Connected = false;
			throw initException;
		}
		catch (Exception e) {
			Log.e(NOMELOG,exceptionStringHelper(e));
			c_Connected = false;
			throw e;
		}
		c_Connected = true;
		
		Message msg = this.c_Handler.obtainMessage();
		
		msg.arg1 = 1; 
		c_Handler.sendMessage(msg);
		
	}
	
	/**
	 * Metodo para desconectar 
	 */
	public synchronized void disconnect() {
		Message msg = this.c_Handler.obtainMessage();
		if ( c_Connected || c_mbTCPMaster.isInitialized() ) {
			if (DEBUGMODE) { Log.i(NOMELOG,"Tentando desconectar!"); }
			c_mbTCPMaster.destroy();
			if (DEBUGMODE) { Log.i(NOMELOG,"Desconectado!"); }
			msg.arg2 = 0;
		} else {
			msg.arg2 = 1;
			if (DEBUGMODE) { Log.i(NOMELOG,"A tentativa de desconex�o falhou!"); }
		}
		c_Connected = false;
		
		msg.arg1 = 0;
		c_Handler.sendMessage(msg);
	}

	/**
	 * 
	 * @return
	 * 
	 * Metodo para verificar se esta conectado
	 */
	public synchronized boolean isConnected() {
		return c_Connected;
	}
	
	public synchronized void writeValue(ModbusMultiLocator writeLocator, Object value) {
		this.w_mbLocator = writeLocator;
		this.w_mbValue = value;
		this.w_mbDoWrite = true;
		this.notify();
	}
	
	@Override
	public void run() {
		
		c_mbValues = null;
		Message msg = this.c_Handler.obtainMessage();
		
		if (!this.isConnected()) {
			try{
				this.connect();
				while(!this.isConnected()) {
					Thread.currentThread();
					Thread.sleep(200);
				}
			}
			catch (RuntimeException runtime_e) {
				Log.e(NOMELOG,exceptionStringHelper(runtime_e));
				msg.arg1 = -1;
				msg.obj = runtime_e.getMessage();
				this.c_Handler.sendMessage(msg);
			}
			catch (Exception connect_e) {
				Log.e(NOMELOG,exceptionStringHelper(connect_e));
				msg.arg1 = -1;
				msg.obj = connect_e.getMessage();
				
				this.c_Handler.sendMessage(msg);
			}
		}
		try {
			while (c_Connected) {
				//Log.i(NOMELOG,"DataType:" + c_mbLocator.getSlaveAndRange() + ", Length: " + c_mbLocator.getLength());
				
				if (DEBUGMODE) { Log.i(NOMELOG, "Lendo Valores");}
				//Adquire os valores do servidor 
				c_mbValues = c_mbTCPMaster.getValues(c_mbLocator);
				activity.runOnUiThread(new Runnable() {
					
					@Override
					public void run() {
						activity.showValues(c_mbValues);
   					}
				});
				
				//O cara diz que precisa sincronizar para funcionar
				if (c_refreshtime != 0) {
					synchronized (this) {
						this.wait(c_refreshtime);
					}
				} else {
					this.disconnect();
				}
				
				//Escrever o valor caso a variavel de escrita for verdadeira
				if (w_mbDoWrite) {
					if (DEBUGMODE) { Log.i(NOMELOG, "Escrevendo valor: " + w_mbValue);
					c_mbTCPMaster.setValue(w_mbLocator, w_mbValue);
					w_mbDoWrite = false;
					}
				}
				
			}
		}
		catch (ModbusTransportException m_exception) {
			Log.e(NOMELOG, exceptionStringHelper(m_exception));
			msg.arg1 = -1;
			msg.obj = m_exception.getMessage();
			c_Handler.sendMessage(msg);
		}
		catch (NullPointerException nullException) {
			Log.e(NOMELOG, "Exception null pointer!" + nullException.getMessage());
			msg.arg1 = -1;
			msg.obj = nullException.getMessage();
			c_Handler.sendMessage(msg);
			this.disconnect();
		}
		catch (Exception e) {
			Log.e(NOMELOG,exceptionStringHelper(e));
			msg.arg1 = -1;
			msg.obj = e.getMessage();
			c_Handler.sendMessage(msg);
		}
		
	}

	private String exceptionStringHelper(Exception e) {
		return (e.getMessage() != null)? e.getMessage() : "";
	}

}
