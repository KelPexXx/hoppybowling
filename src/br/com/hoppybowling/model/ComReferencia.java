package br.com.hoppybowling.model;

public class ComReferencia {
	
	private int Inicial;
	private int Qntidade = 30;
	private int mqLigada;
	private int mqRearme;
	private int mqBola;
	private int iniPinos;
	private int fimPinos;

	private ComReferencia(int Inicial){
		this.Inicial = Inicial;
		CalculaReferencia();
	}

	
	private void CalculaReferencia(){
		mqBola = Inicial;
		iniPinos = Inicial + 1;
		fimPinos = iniPinos + 9;
		mqRearme = fimPinos + 1;
		mqLigada = Inicial + 20;
	}

	public int getInicial() {
		return Inicial;
	}

	public int getQntidade() {
		return Qntidade;
	}

	public int getMqLigada() {
		return mqLigada;
	}

	public int getMqRearme() {
		return mqRearme;
	}

	public int getMqBola() {
		return mqBola;
	}

	public int getIniPinos() {
		return iniPinos;
	}

	public int getFimPinos() {
		return fimPinos;
	}
	
	
}
