package br.com.hoppybowling.adapters;

import java.io.File;
import java.util.ArrayList;
import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import br.com.hoppybowling.R;
import br.com.hoppybowling.model.Jogador;
import br.com.hoppybowling.util.ReferenciasConstantes;


public class CustomAdapter extends BaseAdapter {

	private static ArrayList<Jogador> aJogador;
	
	private LayoutInflater mInflater;
	
	public CustomAdapter(Context context, ArrayList<Jogador> jogadores) {
		aJogador = jogadores;
		mInflater = LayoutInflater.from(context);
	}
	
	@Override
	public int getCount() {
		return aJogador.size();
	}

	@Override
	public Object getItem(int position) {
		return aJogador.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@SuppressLint("InflateParams")
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		if(convertView == null){
			convertView = mInflater.inflate(R.layout.l_row_frames , null);
			holder = new ViewHolder();
			holder.txtNome = (TextView) convertView.findViewById(R.id.txtNome);
			holder.imgFoto = (ImageView) convertView.findViewById(R.id.imgFoto);
			holder.txtframe_f1_j1 = (TextView) convertView.findViewById(R.id.txtframe_f1_j1);
			holder.txtframe_f1_j2 = (TextView) convertView.findViewById(R.id.txtframe_f1_j2);
			holder.txtframe_f1_ftotal = (TextView) convertView.findViewById(R.id.txtframe_f1_ftotal);
			holder.txtframe_f2_j1 = (TextView) convertView.findViewById(R.id.txtframe_f2_j1);
			holder.txtframe_f2_j2 = (TextView) convertView.findViewById(R.id.txtframe_f2_j2);
			holder.txtframe_f2_ftotal = (TextView) convertView.findViewById(R.id.txtframe_f2_ftotal);
			holder.txtframe_f3_j1 = (TextView) convertView.findViewById(R.id.txtframe_f3_j1);
			holder.txtframe_f3_j2 = (TextView) convertView.findViewById(R.id.txtframe_f3_j2);
			holder.txtframe_f3_ftotal = (TextView) convertView.findViewById(R.id.txtframe_f3_ftotal);
			holder.txtframe_f4_j1 = (TextView) convertView.findViewById(R.id.txtframe_f4_j1);
			holder.txtframe_f4_j2 = (TextView) convertView.findViewById(R.id.txtframe_f4_j2);
			holder.txtframe_f4_ftotal = (TextView) convertView.findViewById(R.id.txtframe_f4_ftotal);
			holder.txtframe_f5_j1 = (TextView) convertView.findViewById(R.id.txtframe_f5_j1);
			holder.txtframe_f5_j2 = (TextView) convertView.findViewById(R.id.txtframe_f5_j2);
			holder.txtframe_f5_ftotal = (TextView) convertView.findViewById(R.id.txtframe_f5_ftotal);
			holder.txtframe_f6_j1 = (TextView) convertView.findViewById(R.id.txtframe_f6_j1);
			holder.txtframe_f6_j2 = (TextView) convertView.findViewById(R.id.txtframe_f6_j2);
			holder.txtframe_f6_ftotal = (TextView) convertView.findViewById(R.id.txtframe_f6_ftotal);
			holder.txtframe_f7_j1 = (TextView) convertView.findViewById(R.id.txtframe_f7_j1);
			holder.txtframe_f7_j2 = (TextView) convertView.findViewById(R.id.txtframe_f7_j2);
			holder.txtframe_f7_ftotal = (TextView) convertView.findViewById(R.id.txtframe_f7_ftotal);
			holder.txtframe_f8_j1 = (TextView) convertView.findViewById(R.id.txtframe_f8_j1);
			holder.txtframe_f8_j2 = (TextView) convertView.findViewById(R.id.txtframe_f8_j2);
			holder.txtframe_f8_ftotal = (TextView) convertView.findViewById(R.id.txtframe_f8_ftotal);
			holder.txtframe_f9_j1 = (TextView) convertView.findViewById(R.id.txtframe_f9_j1);
			holder.txtframe_f9_j2 = (TextView) convertView.findViewById(R.id.txtframe_f9_j2);
			holder.txtframe_f9_ftotal = (TextView) convertView.findViewById(R.id.txtframe_f9_ftotal);
			holder.txtframe_f10_j1 = (TextView) convertView.findViewById(R.id.txtframe_f10_j1);
			holder.txtframe_f10_j2 = (TextView) convertView.findViewById(R.id.txtframe_f10_j2);
			holder.txtframe_f10_j3 = (TextView) convertView.findViewById(R.id.txtframe_f10_j3);
			holder.txtframe_f10_ftotal = (TextView) convertView.findViewById(R.id.txtframe_f10_ftotal);
			holder.txtTotal = (TextView) convertView.findViewById(R.id.txtTotal);
			
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		File imgFile = new  File(ReferenciasConstantes.DIR_FOTOS,aJogador.get(position).getAvatar());
	    Bitmap imgAvatar = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
	    
		holder.imgFoto.setImageBitmap(getRoundedBitmap(imgAvatar));
		holder.txtNome.setText(aJogador.get(position).getApelido());		
		holder.txtframe_f1_j1.setText(getTextoJogada(position, 0, 0));
		holder.txtframe_f1_j2.setText(getTextoJogada(position, 0, 1));
		holder.txtframe_f1_ftotal.setText(getTextoTotal(position, 0));
		holder.txtframe_f2_j1.setText(getTextoJogada(position, 1, 0));
		holder.txtframe_f2_j2.setText(getTextoJogada(position, 1, 1));
		holder.txtframe_f2_ftotal.setText(getTextoTotal(position, 1));
		holder.txtframe_f3_j1.setText(getTextoJogada(position, 2, 0));
		holder.txtframe_f3_j2.setText(getTextoJogada(position, 2, 1));
		holder.txtframe_f3_ftotal.setText(getTextoTotal(position, 2));
		holder.txtframe_f4_j1.setText(getTextoJogada(position, 3, 0));
		holder.txtframe_f4_j2.setText(getTextoJogada(position, 3, 1));
		holder.txtframe_f4_ftotal.setText(getTextoTotal(position, 3));
		holder.txtframe_f5_j1.setText(getTextoJogada(position, 4, 0));
		holder.txtframe_f5_j2.setText(getTextoJogada(position, 4, 1));
		holder.txtframe_f5_ftotal.setText(getTextoTotal(position, 4));
		holder.txtframe_f6_j1.setText(getTextoJogada(position, 5, 0));
		holder.txtframe_f6_j2.setText(getTextoJogada(position, 5, 1));
		holder.txtframe_f6_ftotal.setText(getTextoTotal(position, 5));
		holder.txtframe_f7_j1.setText(getTextoJogada(position, 6, 0));
		holder.txtframe_f7_j2.setText(getTextoJogada(position, 6, 1));
		holder.txtframe_f7_ftotal.setText(getTextoTotal(position, 6));
		holder.txtframe_f8_j1.setText(getTextoJogada(position, 7, 0));
		holder.txtframe_f8_j2.setText(getTextoJogada(position, 7, 1));
		holder.txtframe_f8_ftotal.setText(getTextoTotal(position, 7));
		holder.txtframe_f9_j1.setText(getTextoJogada(position, 8, 0));
		holder.txtframe_f9_j2.setText(getTextoJogada(position, 8, 1));
		holder.txtframe_f9_ftotal.setText(getTextoTotal(position, 8));
		holder.txtframe_f10_j1.setText(getTextoJogada(position, 9, 0));
		holder.txtframe_f10_j2.setText(getTextoJogada(position, 9, 1));
		holder.txtframe_f10_j3.setText(getTextoJogada(position, 9, 2));
		holder.txtframe_f10_ftotal.setText(getTextoTotal(position, 9));
		holder.txtTotal.setText(String.valueOf(aJogador.get(position).getTotal()));
		return convertView;
	}
	
	static class ViewHolder{
		TextView txtNome;
		ImageView imgFoto;
		TextView txtframe_f1_j1;
		TextView txtframe_f1_j2;
		TextView txtframe_f1_ftotal;
		TextView txtframe_f2_j1;
		TextView txtframe_f2_j2;
		TextView txtframe_f2_ftotal;
		TextView txtframe_f3_j1;
		TextView txtframe_f3_j2;
		TextView txtframe_f3_ftotal;
		TextView txtframe_f4_j1;
		TextView txtframe_f4_j2;
		TextView txtframe_f4_ftotal;
		TextView txtframe_f5_j1;
		TextView txtframe_f5_j2;
		TextView txtframe_f5_ftotal;
		TextView txtframe_f6_j1;
		TextView txtframe_f6_j2;
		TextView txtframe_f6_ftotal;
		TextView txtframe_f7_j1;
		TextView txtframe_f7_j2;
		TextView txtframe_f7_ftotal;
		TextView txtframe_f8_j1;
		TextView txtframe_f8_j2;
		TextView txtframe_f8_ftotal;
		TextView txtframe_f9_j1;
		TextView txtframe_f9_j2;
		TextView txtframe_f9_ftotal;
		TextView txtframe_f10_j1;
		TextView txtframe_f10_j2;
		TextView txtframe_f10_j3;
		TextView txtframe_f10_ftotal;
		TextView txtTotal;
	}
	
	private String getTextoJogada(int posicao, int frame, int jogada){
		String texto;
		Integer valor = aJogador.get(posicao).getJogada(frame, jogada);
		if (valor == null){
			texto = " ";
		} else {
			texto =String.valueOf(valor);
		}
		return texto;
	}
	
	private String getTextoTotal(int posicao, int frame){
		String texto;
		Integer valor = aJogador.get(posicao).getTotalFrame(frame);
		if (valor == null){
			texto = " ";
		} else {
			texto =String.valueOf(valor);
		}
		return texto;
	}
	public static final Bitmap getRoundedBitmap(Bitmap bitmap) {
	    final Bitmap output = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
	    final Canvas canvas = new Canvas(output);
	 
	    final int color = Color.RED;
	    final Paint paint = new Paint();
	    final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
	    final RectF rectF = new RectF(rect);
	 
	    paint.setAntiAlias(true);
	    canvas.drawARGB(0, 0, 0, 0);
	    paint.setColor(color);
	    canvas.drawOval(rectF, paint);
	 
	    paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
	    canvas.drawBitmap(bitmap, rect, rect, paint);
	 
	    bitmap.recycle();
	 
	    return output;
	  }	
	
}

