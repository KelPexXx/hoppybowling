package br.com.hoppybowling.activities;

import android.app.Activity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.SeekBar;
import android.widget.TextView;

public class CustomActivity extends Activity {
		
		private View mDecorView;
		TextView mText;
	    TextView mTitleView;
	    SeekBar mSeekView;
	    boolean mNavVisible;
	    int mBaseSystemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
	            | View.SYSTEM_UI_FLAG_LAYOUT_STABLE;
	    int mLastSystemUiVis;
	
 
	    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		hideSystemUI();
		mDecorView = getWindow().getDecorView();
		//mDecorView.setOnSystemUiVisibilityChangeListener(this);
		
	}
	
	
	

	private void hideSystemUI() {
		View mDecorView = getWindow().getDecorView();
		mDecorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
				| View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
				| View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
				| View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
				| View.SYSTEM_UI_FLAG_LOW_PROFILE
				// hide nav bar
				| View.SYSTEM_UI_FLAG_FULLSCREEN);
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			// Do Code Here
			// If want to block just return false
			//return false;
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
		


}