package br.com.hoppybowling.model;

import java.util.ArrayList;

public class Pinos {
	private ArrayList<Integer> pinos;
	
	public Pinos(){
		pinos = new ArrayList<Integer>();
		for (int i = 0; i <= 9; i++){
			pinos.add(0);
		}
	}
	
	public void setPino(int pino,int valor){
		pinos.set(pino, valor);
	}
	
	public Integer getPino(int pino){
		return pinos.get(pino).intValue();
	}
	
	public void setPinos (ArrayList<Integer> pinos){
		this.pinos = pinos;
	}
	
	public ArrayList<Integer> getPinos(){
		return pinos;
	}
	
	public Integer getTotal() {
		int total = 0;
		for (int i = 0; i <= 9; i++){
			total = total + pinos.get(i).intValue();
		}
		return total;
	}
}