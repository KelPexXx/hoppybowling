package br.com.hoppybowling.model;

import java.io.File;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import br.com.hoppybowling.R;

/**
 * 
 * @author Rafael Wermer
 *
 *	Classe AlertaPersonalizado
 */
public class AlertaPersonalizado {
	
	private Toast tAlerta;
	private View tAlertaView;
	
	private ImageView imgIcone;
	private ImageView imgMask;
	private TextView txtTitulo;
	private TextView txtMsg1;
	private TextView txtMsg2;
	private FrameLayout frmLayout;

	public AlertaPersonalizado(Context context,LayoutInflater layoutinflater, int layout, int background){

        LayoutInflater inflater = layoutinflater;

        tAlertaView = inflater.inflate(layout, null);
        tAlertaView.setBackgroundResource(background);
        
        tAlerta = new Toast(context);
        
        imgIcone = (ImageView) tAlertaView.findViewById(R.id.imgIcone);
        imgMask = (ImageView) tAlertaView.findViewById(R.id.imgMask);
        txtTitulo = (TextView) tAlertaView.findViewById(R.id.txt_title_toast);
        txtMsg1 = (TextView) tAlertaView.findViewById(R.id.txt_msg1_toast);
        txtMsg2 = (TextView) tAlertaView.findViewById(R.id.txt_msg2_toast);
        frmLayout = (FrameLayout) tAlertaView.findViewById(R.id.frmImg);
	}
	
	public void ExibirAlerta(int gravidade, int duracao){
		tAlerta.setView(tAlertaView);
        tAlerta.setGravity(gravidade,0, 0);
        tAlerta.setDuration(duracao);
        tAlerta.show();
	}
	
	public void setImgVisivel(boolean estado){
		if (estado){
			frmLayout.setVisibility(View.VISIBLE);
		} else {
			frmLayout.setVisibility(View.GONE);
		}
	}

	public void setImgIconeVisivel(boolean estado){
		if (estado){
			imgIcone.setVisibility(View.VISIBLE);
		} else {
			imgIcone.setVisibility(View.GONE);
		}
	}
	
	public void setImgIcone(String img){
		File imgTemp = new File(img);
		imgIcone.setImageURI(Uri.fromFile(imgTemp));
	}
	
	public void setImgIcone(File img){
		imgIcone.setImageURI(Uri.fromFile(img));
	}
	
	public void setImgIcone(Bitmap img){
		imgIcone.setImageBitmap(img);
	}	

	public void setImgIcone(Drawable img){
		imgIcone.setImageDrawable(img);
	}

	public void setImgMaskVisivel(boolean estado){
		if (estado){
			imgMask.setVisibility(View.VISIBLE);
		} else {
			imgMask.setVisibility(View.GONE);
		}
	}	
	
	public void setImgMask(String img){
		File imgTemp = new File(img);
		imgMask.setImageURI(Uri.fromFile(imgTemp));
	}
	
	public void setImgMask(File img){
		imgMask.setImageURI(Uri.fromFile(img));
	}
	
	public void setImgMask(Bitmap img){
		imgMask.setImageBitmap(img);
	}		

	public void setImgMask(Drawable img){
		imgMask.setImageDrawable(img);
	}

	public void setTituloVisivel(boolean estado){
		if (estado){
			txtTitulo.setVisibility(View.VISIBLE);
		} else {
			txtTitulo.setVisibility(View.INVISIBLE);
		}
	}		
	
	public void setTitulo(String titulo){
		txtTitulo.setText(titulo);
	}
	
	public void setTituloColor(int color){
		txtTitulo.setTextColor(color);
		txtTitulo.setShadowLayer(10, 2, 2, color);
	}

	public void setMsg1Visivel(boolean estado){
		if (estado){
			txtMsg1.setVisibility(View.VISIBLE);
		} else {
			txtMsg1.setVisibility(View.INVISIBLE);
		}
	}			
	
	public void setMsg1(String texto){
		txtMsg1.setText(texto);
	}
	
	public void setMsg1Color(int color){
		txtMsg1.setTextColor(color);
	}
	
	public void setMsg2Visivel(boolean estado){
		if (estado){
			txtMsg2.setVisibility(View.VISIBLE);
		} else {
			txtMsg2.setVisibility(View.INVISIBLE);
		}
	}		
	
	public void setMsg2(String texto){
		txtMsg2.setText(texto);
	}
	
	public void setMsg2Color(int color){
		txtMsg2.setTextColor(color);
	}	
}