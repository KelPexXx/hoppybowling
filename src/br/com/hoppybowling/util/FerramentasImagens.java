package br.com.hoppybowling.util;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;

public class FerramentasImagens {
	
	private Bitmap imgOrig;
	
	public FerramentasImagens(Bitmap imgOrig) {
		this.imgOrig = imgOrig;
	}
	
	public Bitmap getImagenRedonda() {
	    final Bitmap output = Bitmap.createBitmap(imgOrig.getWidth(), imgOrig.getHeight(), Bitmap.Config.ARGB_8888);
	    final Canvas canvas = new Canvas(output);
	 
	    final int color = Color.RED;
	    final Paint paint = new Paint();
	    final Rect rect = new Rect(0, 0, imgOrig.getWidth(), imgOrig.getHeight());
	    final RectF rectF = new RectF(rect);
	 
	    paint.setAntiAlias(true);
	    canvas.drawARGB(0, 0, 0, 0);
	    paint.setColor(color);
	    canvas.drawOval(rectF, paint);
	 
	    paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
	    canvas.drawBitmap(imgOrig, rect, rect, paint);
	 
	    imgOrig.recycle();
	 
	    return output;
	  }
	
	

	
}
