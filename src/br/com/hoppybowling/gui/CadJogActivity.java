package br.com.hoppybowling.gui;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.Camera;
import android.hardware.Camera.PictureCallback;
import android.hardware.Camera.Size;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.Toast;
import br.com.hoppybowling.R;
import br.com.hoppybowling.model.AlertaPersonalizado;
import br.com.hoppybowling.model.Jogador;
import br.com.hoppybowling.model.ShowCamera;
import br.com.hoppybowling.util.FerramentasImagens;
import br.com.hoppybowling.util.ReferenciasConstantes;

public class CadJogActivity extends Activity {
	
	public int nJogadores = -1;
	
	private Button btn_cj_Cadastrar;
	private Button btn_cj_Prosseguir;
	private EditText edt_cj_NomeJogador;
	private EditText edt_cj_ApelidoJogador;
	private ArrayList<Jogador> cJogadores;
	
	private FrameLayout preview;
	private Camera cCamera;
	private ShowCamera scCamera;
	
	private AlertaPersonalizado alCadJogOk;
	private AlertaPersonalizado alCadJogMax;
	private AlertaPersonalizado alCadJogMin;
	
	private int CropX;
	private int CropY;
	private int CropNX;
	private int CropNY;
	
	public Camera isCameraAvaliable() {
		Camera object = null;
		int nCameras = Camera.getNumberOfCameras() ; 
		if (nCameras > 0) {	
			try{
				object = Camera.open(nCameras-1);
			} catch (Exception e){
				Log.e("Erro", "Camera n�o encontrada!");
			}
		} else {
			Toast.makeText(this, getResources().getString(R.string.tx_erro_camera), Toast.LENGTH_LONG).show();
		}
		return object;
	}
	
	private PictureCallback captureIt = new PictureCallback() {
		
		@Override
		public void onPictureTaken(byte[] data,Camera camera){
			Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
			
			Bitmap cropBt = Bitmap.createBitmap(bitmap,CropX,CropY,CropNX,CropNY);
			try {
				FileOutputStream outStream = null; // outstream de saida
				File iPhoto = new File(ReferenciasConstantes.DIR_FOTOS,ReferenciasConstantes.NOME_FOTOS[nJogadores]);
				outStream = new FileOutputStream(iPhoto);  // associa o outstream com o arquivo criado
				cropBt.compress(Bitmap.CompressFormat.JPEG, 40, outStream);  // passa o Bitmap para o outstream, ou seja, para o arquivo
				outStream.close();
				iPhoto = null;
			} catch (FileNotFoundException e) { 
				e.printStackTrace(); 
			} catch (IOException e) { 
				e.printStackTrace(); 
			} finally {
				bitmap.recycle();
				FerramentasImagens imgRedonda = new FerramentasImagens(cropBt);
				alCadJogOk.setImgIcone(imgRedonda.getImagenRedonda());
				alCadJogOk.ExibirAlerta(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL, Toast.LENGTH_SHORT);
				//showCustomAlert(cropBt);
				bitmap = null;
				cropBt = null;
				camera.startPreview();
			}
		}
	};
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_cadjog);
		
		getCamera();
		getCropResolucao(cCamera.getParameters().getPictureSize());
		
		btn_cj_Cadastrar = (Button) findViewById(R.id.btn_cj_Cadastrar);
		btn_cj_Prosseguir = (Button) findViewById(R.id.btn_cj_IniciarJogo);
		edt_cj_NomeJogador = (EditText) findViewById(R.id.edt_cj_NomeJogador);
		edt_cj_ApelidoJogador = (EditText) findViewById(R.id.edt_cj_ApelidoJogador);
		
		cJogadores = new ArrayList<Jogador>();
		
		edt_cj_ApelidoJogador.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				if(edt_cj_ApelidoJogador.getText().length() > 10){
					Toast.makeText(getApplicationContext(), "Somente 10 caracteres permitidos!", Toast.LENGTH_SHORT).show();
					edt_cj_ApelidoJogador.setText(s.toString().substring(0, 10));
				}
				
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				if(edt_cj_ApelidoJogador.getText().length() == 10){
					btn_cj_Cadastrar.requestFocus();
				}
				
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				
			}
		});
		
		btn_cj_Cadastrar.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(nJogadores<=4){
					nJogadores++;
					Jogador jg = new Jogador();
					if (edt_cj_NomeJogador.getText().length() > 0 || 
							edt_cj_ApelidoJogador.getText().length() > 0) {
						if (edt_cj_NomeJogador.getText().length() == 0){
							jg.setNome(edt_cj_ApelidoJogador.getText().toString());
							jg.setApelido(edt_cj_ApelidoJogador.getText().toString());
						} else if (edt_cj_ApelidoJogador.getText().length() == 0){
							String Nome = edt_cj_NomeJogador.getText().toString(); 
							jg.setNome(Nome);
							Nome = Nome + " ";
							int Pos;
							if (Nome.indexOf(" ") > 10){
								Pos = 9;
							} else {
								Pos = Nome.indexOf(" ");
							}
							jg.setApelido(Nome.substring(0, Pos));
						} else {
							jg.setNome(edt_cj_NomeJogador.getText().toString());
							jg.setApelido(edt_cj_ApelidoJogador.getText().toString());
						}
					} else {
						jg.setNome(" ");
						jg.setApelido(" ");
					}
					jg.setAvatar(ReferenciasConstantes.NOME_FOTOS[nJogadores]);
					cJogadores.add(jg);
					alCadJogOk.setMsg1("Bem vindo " + jg.getApelido() + "!");
					alCadJogOk.setMsg2((nJogadores+1) + "/6");
					edt_cj_NomeJogador.setText("");
					edt_cj_ApelidoJogador.setText("");
					edt_cj_NomeJogador.requestFocus();
					snapIt();
				} else {
					alCadJogMax.ExibirAlerta(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL, Toast.LENGTH_LONG);	
				}
			
			}
		});
		
		btn_cj_Prosseguir.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if (nJogadores >= 0){
					Intent iPartida = new Intent(CadJogActivity.this,PartidaActivity.class);
					iPartida.putExtra("Lista_Jogadores", cJogadores);
					startActivity(iPartida);
					finish();
				} else {
					alCadJogMin.ExibirAlerta(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL, Toast.LENGTH_LONG);
				}
			}
		});
		
		//Definindo Alertas Personalizados
		alCadJogOk = new AlertaPersonalizado(this, getLayoutInflater(), R.layout.l_alerta_personalizado, R.drawable.i_flat_alerta_fundo_branco);
		alCadJogOk.setTitulo("Jogador Adicionado!");
		alCadJogOk.setImgVisivel(true);
		alCadJogOk.setImgMask(getResources().getDrawable(R.drawable.i_flat_alerta_mask_jogador));
		
		alCadJogMax = new AlertaPersonalizado(this, getLayoutInflater(), R.layout.l_alerta_personalizado, R.drawable.i_flat_alerta_fundo_branco);
		alCadJogMax.setTitulo("Aten��o!");
		alCadJogMax.setTituloColor(getResources().getColor(R.color.vermelho));
		alCadJogMax.setMsg1("O n�mero m�ximo de jogadores foi atingido!");
		alCadJogMax.setMsg2("Somente 6 jogadores por partida!");
		
		alCadJogMin = new AlertaPersonalizado(this, getLayoutInflater(), R.layout.l_alerta_personalizado, R.drawable.i_flat_alerta_fundo_branco);
		alCadJogMin.setTitulo("Aten��o!");
		alCadJogMin.setTituloColor(getResources().getColor(R.color.amarelo));
		alCadJogMin.setMsg1("� necessario cadastrar pelo menos um jogador!");
		alCadJogMin.setMsg2("No m�ximo 6 jogadores por partida!");
		
	}

	@Override
	protected void onRestart() {
		super.onRestart();
	}

	@Override
	protected void onResume() {
		super.onResume();
		if(cCamera == null){
			getCamera();
		}
	}

	@Override
	protected void onPause() {
		super.onPause();
		cCamera = null;
	}

	@Override
	protected void onStop() {
		super.onStop();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}
	
	private void getCamera() {
		cCamera = isCameraAvaliable();
		scCamera = new ShowCamera(this, cCamera);
		preview = (FrameLayout) findViewById(R.id.cj_CameraPreview);
		preview.addView(scCamera);
	}
	
	public void snapIt(){
		cCamera.takePicture(null, null, captureIt);
	}
	
	private void getCropResolucao(Size size){
		int width = size.width;
		int height = size.height;
		
		CropY = 0;
		CropX = (int) ((width - height) / 2);
		CropNX = height;
		CropNY = height;
	}
	
	
}
