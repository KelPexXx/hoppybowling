package br.com.hoppybowling.model;

import java.io.Serializable;
import java.util.ArrayList;

public class Jogador implements Serializable {
	
	private static final long serialVersionUID = 1L;
	/**
	 * HoppyBowling
	 * 25/08/14
	 * Rafael Gustavo Simon Werner 
	 * 
	 * Classe responsavel por armazenar as informa�oes e pontua��es de cada jogador;
	 * 
	 * Variavel do tipo String para armazenar o nome completo do jogador;
	 * Variavel do tipo String para armazenar o apelido do jogador;
	 * Variavel para armazenar o id da foto/avatar selecionada pelo jogadro; 
	 * Variavel para armazenar o total de pontos do jogador; 
	 * Array que armazena  as jogadas do jogador; 
	 */  
	
	private String nome;
    private String apelido; 
    private String avatar; 
    private int total; 

    private ArrayList<Frame> frames; 

    
    /**
     * Metodo Construtor ja inicia o array com as 10 jogadas;
     */
    public Jogador() {
        frames = new ArrayList<Frame>();
        total = 0;
        initArray();
        // TODO para fazer a cria��o de 10 frames
    }

    /**
     * Metodo construtor para iniciar com um numero de jogadas especificado;
     * 
     * @param nFrames
     */
    public Jogador(int nFrames) {
        frames = new ArrayList<Frame>();
        total = 0;
        initArray();
        // TODO para fazer a cria��o de 10 frames
    }

    /**
     * Metodo construtor para iniciar o jogador com nome,apelido,avatar definido;
     * 
     * @param nFrames
     * @param nome
     * @param apelido
     * @param avatar
     */
    public Jogador(int nFrames, String nome, String apelido, String avatar) {
        frames = new ArrayList<Frame>();
        initArray();
        this.nome = nome;
        this.apelido = apelido;
        this.avatar = avatar;
        total = 0;
        // TODO para fazer a cria��o de 10 frames
    }
    
    /**
     * Metodo para definir o nome do jogador;
     * @param nome
     */
    public void setNome(String nome){
    	this.nome = nome;
    }
    
    /**
     * Metodo para retornar o nome do jogador;
     * @return
     */
    public String getNome(){
    	return nome;
    }
    
    /**
     * Metodo para definir o apelido do jogador;    
     * @param apelido
     */
    public void setApelido(String apelido){
    	this.apelido = apelido;
    }
    
    /**
     * Metodo para retornar o apelido do jogador;
     * @return
     */
    public String getApelido(){
    	return apelido;
    }
    
    /**
     * Metodo para definir o avatar/foto do jogador;
     * @param avatar
     */
    public void setAvatar(String avatar){
    	this.avatar = avatar;
    }
    
    /**
     * Metodo para retornar o avatar/foto do jogador;
     * @return
     */
    public String getAvatar(){
    	return avatar;
    }
 
    /**
     * Metodo para definir a os pontos da jogada efetuada pelo jogador,
     *	recebe o numero do frame a jogada do frame e os pontos feitos pelo jogador;
     * 
     * @param frame
     * @param jogada
     * @param pontos
     */
    public void setJogada(int frame, int jogada, int pontos) {
        this.frames.get(frame).setJogada(jogada, pontos);
    }

    /**
     * Metodo para recuperar os pontos de uma jogada especifica;
     * 
     * @param frame
     * @param jogada
     * @return
     */
    public Integer getJogada(int frame, int jogada) {
        return this.frames.get(frame).getJogada(jogada);
    }
    
    public void setPinoJogada(int frame,int jogada, int pino, int valor){
    	this.frames.get(frame).setPino(jogada, pino, valor);
    }
    
    public Integer getPinoJogada(int frame,int jogada, int pino){
    	return this.frames.get(frame).getPino(jogada,pino);
    }
    
    public void setPinosJogada(int frame, int jogada, ArrayList<Integer> pinos){
    	this.frames.get(frame).setPinos(jogada, pinos);
    }
    
    public ArrayList<Integer> getPinosJogada(int frame, int jogada){
    	return this.frames.get(frame).getPinos(jogada);
    }
    
    public Integer getTotalPinos(int frame, int jogada){
    	return this.frames.get(frame).getTotalPinos(jogada);
    }
    
    /**
     * Metodo para definir o total do frame;
     * 
     * @param frame
     * @param total
     */
    public void setTotalFrame(int frame, int total){
    	this.frames.get(frame).setTotal(total);
    }
    
    /**
     * Metodo para somar os pontos ao total do frame;
     * 
     * @param frame
     * @param pontos
     */
    public void somaTotalFrame(int frame, int pontos){
    	this.frames.get(frame).somaTotal(pontos);
    }
    
    /**
     * Metodo para retornar o total do frame;
     * 
     * @param frame
     * @return
     */
    public Integer getTotalFrame(int frame){
    	return this.frames.get(frame).getTotal();
    }

    /**
     * Metodo para definir o total do jogador;
     * 
     * @param total
     */
    public void setTotal(int total){
    	this.total = total;
    }
    
    /**
     * Metodo para adicionar os pontos ao total do jogador;
     * 
     * @param total
     */
    public void addTotal(int total){
    	this.total = this.total + total;
    }
    
    /**
     * Metodo para retornar o total do jogador;
     * 
     * @return
     */
    public Integer getTotal(){
    	return total;
    }
    
    public void updateTotal(){
    	int auxTotal = 0;
    	for (int i=0;i<=9;i++){
    		int auxSoma = 0;
    		Integer auxJogada1 = frames.get(i).getJogada(0);
    		Integer auxJogada2 = frames.get(i).getJogada(1);
    		if ((auxJogada1 == null)||(auxJogada2 == null)){
    			break;
    		} else {
    			if(auxJogada1 == 10){
    				//Strike
    				auxSoma = auxJogada1;
    			} else {
    				auxSoma = auxJogada1 + auxJogada2;
	    			if (auxSoma == 10){
	    				//Spare
	    			}
    			}
    		}
    		auxTotal = auxTotal + auxSoma;
    	}
    }
    
    public void initArray(){
    	Frame frame;
    	for (int i=0; i<=9; i++){
    		frame = new Frame(i);
    		frames.add(frame);
    	}
    }
}
