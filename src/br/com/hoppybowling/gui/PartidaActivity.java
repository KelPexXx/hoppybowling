package br.com.hoppybowling.gui;

import java.util.ArrayList;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;
import br.com.hoppybowling.R;
import br.com.hoppybowling.adapters.CustomAdapter;
import br.com.hoppybowling.modbus.ModbusConnectionManager;
import br.com.hoppybowling.model.Jogador;

import com.bencatlin.modbusdroid.OldVersion.MbDroidMsgExceptionHandler;
import com.bencatlin.modbusdroid.OldVersion.ModbusMultiLocator;
import com.bencatlin.modbusdroid.OldVersion.ModbusTCPFactory;
import com.bencatlin.modbusdroid.OldVersion.ModbusTCPMaster;
import com.serotonin.modbus4j.ip.IpParameters;

public class PartidaActivity extends Activity {
	
	private static final boolean FAZLOG = true;
	private static final String NOMELOG = "HoppyBowling_log"; 
	
	private static final String IP_CLP = "192.168.0.104";
	private static final int PORTA_CLP = 502;
	private static final int HOLDREG_INICIAL = 0;
	private static final int HOLDREG_TAMANHO = 30;

	private IpParameters ipParameters;
	private ModbusTCPFactory mbFactory;
	private ModbusTCPMaster mbTCPMaster;
	private ModbusMultiLocator mbLocator;
	private ModbusConnectionManager mb = null;
	
	private MbDroidMsgExceptionHandler exceptionHandler;
	
	Thread mbThread = null;
	
	public ArrayList<Jogador> cJogador;
	public CustomAdapter caJogadores;
	
	public Handler ComHandler = new Handler () {
		
		@Override
		public void handleMessage (Message Message) {
			int arg1 = Message.arg1;
			int arg2 = Message.arg2;
			String msgString = (String) Message.obj;
			String textString = null;
		switch (arg1) {
			case 0:
				switch (arg2) {
				case 0:
					textString = "Desconectou!";
					break;
				case 1:
					textString = "N�o esta conectado!";
					break;
				default:
					textString = "Erro!!!";
					break;
				}
				break;
			case 1:
				textString = "Conectado!";
				break;
			case 2:
					
				break;
			case -1:
				textString = "Erro: " + msgString;
			default:
				textString = "Erro!!";
				break;
		}
		Toast.makeText(getBaseContext(), textString, Toast.LENGTH_LONG).show();
		super.handleMessage(mdMessage);
		}
	};
	
	
	
	
	
	
	@SuppressWarnings("unchecked")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		hideSystemUI();
		
		setContentView(R.layout.activity_partida);
		
		
		//ArrayList<Jogador> cJogador = GetJogadores();
		cJogador = (ArrayList<Jogador>) getIntent().getSerializableExtra("Lista_Jogadores");
		final ListView lv = (ListView) findViewById(R.id.lvJogadores);
		caJogadores = new CustomAdapter(this,cJogador);
		lv.setAdapter(caJogadores);
	}
	
	
	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.partida, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	
	
	
	private void hideSystemUI() {
		View mDecorView = getWindow().getDecorView();
		mDecorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
				| View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
				| View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
				| View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
				| View.SYSTEM_UI_FLAG_LOW_PROFILE
				// hide nav bar
				| View.SYSTEM_UI_FLAG_FULLSCREEN
				| View.SYSTEM_UI_FLAG_IMMERSIVE);
	}
}
