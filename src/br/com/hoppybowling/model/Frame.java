package br.com.hoppybowling.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * 
 * HoppyBowling
 * 25/08/14
 * 
 * @author Rafael Werner
 *
 * Classe responsavel por armazenar as informaçoes e pontuações de cada jogador;
 * 
 */
public class Frame implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * Variavel do tipo int para armazenar o numero do Frame (0 - 10);
	 * Array para armazenar as 2-3 jogadas do Frame;
	 * Variavel do tipo int para armazenar o total ate o Frame;
	 */
	
	private int numero;
    private ArrayList<Integer> jogadas;
    private ArrayList<Pinos> pinos;
    private Integer total;

    /**
     * Construtor ja cria o ArrayList com as 2 jogadas zeradas;
     */
    public Frame() {
        jogadas = new ArrayList<Integer>();
        total = null;
        initArray();
    }
    
    /**
     * Contrutor cria o ArrayList com as 2 jogadas zeradas e define
     * o numero do frame;
     * 
     * @param numero
     */
    public Frame(int numero){
    	jogadas = new ArrayList<Integer>();
    	pinos = new ArrayList<Pinos>();
    	total = null;
    	initArray();
    	this.numero = numero;
    	if (numero == 9){
    		addJogada(null);
    	}
    }
    
    public Integer getNumero(){
    	return this.numero;
    }

    /**
     * Metodo para adicionar a terceira jogada utilizada no frame de numero 10;
     * 
     * @param pontos
     */
    public void addJogada(Integer pontos) {
        jogadas.add(pontos);
    }

    /**
     * Metodo para pegar o valor de uma das jogadas do frame;
     * 
     * @param jogada
     * @return
     */
    public Integer getJogada(int jogada) {
        return jogadas.get(jogada);
    }

    /** 
     * Metodo para definir o valor de uma das jogadas do frame;
     * 
     * @param jogada
     * @param pontos
     */
    public void setJogada(int jogada, int pontos) {
        jogadas.set(jogada, pontos);
    }
    
    
    public void setPino(int jogada, int pino, int valor){
    	pinos.get(jogada).setPino(pino, valor);
    }
    
    public void setPinos(int jogada, ArrayList<Integer> aPinos){
    	pinos.get(jogada).setPinos(aPinos);
    }
    
    public Integer getPino(int jogada, int aPino){
    	return pinos.get(jogada).getPino(aPino);
    }
    
    public ArrayList<Integer> getPinos(int jogada){
    	return pinos.get(jogada).getPinos();
    }
    
    public Integer getTotalPinos(int jogada){
    	return pinos.get(jogada).getTotal();
    }
    
    /**
     * Metodo para definir o total de pontos feitos ate o frame;
     * 
     * @param pontos
     */
    public void setTotal(int pontos){
    	total = pontos;
    }
    
    /**
     * Metodo para somar os pontos feitos ao total do frame;
     * 
     * @param pontos
     */
    public void somaTotal(int pontos){
    	total = total + pontos;
    }
    
    /**
     * Metodo para retornar o total atual do frame;
     * 
     * @return
     */
    public Integer getTotal(){
    	return total;
    }  
    
    public void initArray(){
    	for (int i=0; i<=1; i++){
    		jogadas.add(null);
    	}
    }
}
