package br.com.hoppybowling.gui;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import br.com.hoppybowling.R;

public class Menu_Principal extends Activity {

	Button btn_mp_Jogo;
	Button btn_mp_Equipe;
	Button btn_mp_Tempo;
	Button btn_mp_Conf;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_menu_principal);
		
		btn_mp_Jogo = (Button) findViewById(R.id.btn_mp_Jogo);
		btn_mp_Equipe = (Button) findViewById(R.id.btn_mp_Equipe);
		btn_mp_Tempo = (Button) findViewById(R.id.btn_mp_Tempo);
		btn_mp_Conf = (Button) findViewById(R.id.btn_mp_Conf);
		
		alteraFonte();
		
		btn_mp_Jogo.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent it_CadJog = new Intent(getApplicationContext(),CadJogActivity.class);
				startActivity(it_CadJog);
			}
		});
	
	}
	
	protected void alteraFonte(){
		Typeface fontePadrao = Typeface.createFromAsset(this.getAssets(), "fonts/blackblox.ttf");
		
		btn_mp_Jogo.setTypeface(fontePadrao);
		btn_mp_Equipe.setTypeface(fontePadrao);
		btn_mp_Tempo.setTypeface(fontePadrao);
		btn_mp_Conf.setTypeface(fontePadrao);
		
	}

}
